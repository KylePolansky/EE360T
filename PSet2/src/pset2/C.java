package pset2;
public class C {
	String s;
	public C(String s) {
		this.s = s;
	}

	@Override
	public boolean equals(Object o) {
// returns a boolean consistent with the Java contract for equals method;
// returns true if and only if o is an object of class
// C and has the same value for field s as this.s (w.r.t. String equals)
// your code goes here
		if (o == null)
			return false;
		if (!this.getClass().equals(o.getClass()))
			return false;

		C that = (C) o;
		return this.s.equals(that.s);
	}

	@Override
	public int hashCode() {
// returns an integer consistent with the Java contract for hashCode method;
// does not return a constant value
// your code goes here
		return (s == null) ? 0 : s.hashCode();
	}
}