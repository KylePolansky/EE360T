package pset2;
import static org.junit.Assert.*;
import org.junit.Test;
public class HashCodeTester {
/*
* P5: If two objects are equal according to the equals(Object)
* method, then calling the hashCode method on each of
* the two objects must produce the same integer result.
*/
// your test methods go here

	final String testString = "test";
	final String testString2 = "test2";
	final int testInt = 0;
	final int testInt2 = 1;

	@Test public void t0() {
		C c = new C(testString);
		assertTrue(c.hashCode() == c.hashCode());
	}

	@Test public void t1() {
		C c = new C(testString);
		C c2 = new C(testString);
		assertTrue(c.hashCode() == c2.hashCode());
	}

	@Test public void t2() {
		C c = new C(testString);
		C c2 = new C(testString2);

		assertFalse(c.hashCode() == c2.hashCode());
	}

	@Test public void t3() {
		C c = new C(null);
		C c2 = new C(null);

		assertTrue(c.hashCode() == c2.hashCode());
	}

	@Test public void t4() {
		C c = new C(null);
		C c2 = new C(testString);

		assertFalse(c.hashCode() == c2.hashCode());
	}

	@Test public void t5() {
		D d = new D(testString, testInt);
		assertTrue(d.hashCode() == d.hashCode());
	}

	@Test public void t6() {
		D d = new D(testString, testInt);
		D d2 = new D(testString, testInt);

		assertTrue(d.hashCode() == d2.hashCode());
	}

	@Test public void t7() {
		D d = new D(testString, testInt);
		D d2 = new D(testString2, testInt);

		assertFalse(d.hashCode() == d2.hashCode());
	}

	@Test public void t8() {
		D d = new D(testString, testInt);
		D d2 = new D(testString, testInt2);

		assertFalse(d.hashCode() == d2.hashCode());
	}

	@Test public void t9() {
		D d = new D(null, testInt);
		D d2 = new D(null, testInt);

		assertTrue(d.hashCode() == d2.hashCode());
	}

	@Test public void t10() {
		D d = new D(null, testInt);
		D d2 = new D(null, testInt2);

		assertFalse(d.hashCode() == d2.hashCode());
	}
}