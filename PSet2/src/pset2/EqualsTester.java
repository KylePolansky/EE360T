package pset2;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class EqualsTester {
	final static String testString = "testString";
	final static int testInt = 1;

	@Parameterized.Parameters public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
				{ new Object(), new Object(), new Object() },
				{ new Object(), new Object(), new C(testString) },
				{ new Object(), new Object(), new D(testString, testInt) },
				{ new Object(), new C(testString), new Object() },
				{ new Object(), new C(testString), new C(testString) },
				{ new Object(), new C(testString), new D(testString, testInt) },
				{ new Object(), new D(testString, testInt), new Object() },
				{ new Object(), new D(testString, testInt), new C(testString) },
				{ new Object(), new D(testString, testInt), new D(testString, testInt) },
				{ new C(testString), new Object(), new Object() },
				{ new C(testString), new Object(), new C(testString) },
				{ new C(testString), new Object(), new D(testString, testInt) },
				{ new C(testString), new C(testString), new Object() },
				{ new C(testString), new C(testString), new C(testString) },
				{ new C(testString), new C(testString), new D(testString, testInt) },
				{ new C(testString), new D(testString, testInt), new Object() },
				{ new C(testString), new D(testString, testInt), new C(testString) },
				{ new C(testString), new D(testString, testInt), new D(testString, testInt) },
				{ new D(testString, testInt), new Object(), new Object() },
				{ new D(testString, testInt), new Object(), new C(testString) },
				{ new D(testString, testInt), new Object(), new D(testString, testInt) },
				{ new D(testString, testInt), new C(testString), new Object() },
				{ new D(testString, testInt), new C(testString), new C(testString) },
				{ new D(testString, testInt), new C(testString), new D(testString, testInt) },
				{ new D(testString, testInt), new D(testString, testInt), new Object() },
				{ new D(testString, testInt), new D(testString, testInt), new C(testString) },
				{ new D(testString, testInt), new D(testString, testInt), new D(testString, testInt) }
		});
	}

	private Object obj1, obj2, obj3;

	public EqualsTester(Object o1, Object o2, Object o3) {
		obj1 = o1;
		obj2 = o2;
		obj3 = o3;
	}

	@Test public void test() {
		if (obj1.equals(obj2) && obj2.equals(obj3))
			assertTrue(obj1.equals(obj3));
	}
}