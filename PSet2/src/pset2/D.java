package pset2;

public class D extends C {
	int g;

	public D(String s, int g) {
		super(s);
		this.g = g;
	}

	@Override
	public boolean equals(Object o) {
// returns a boolean consistent with the Java contract for equals method;
// returns true if and only if o is an object of class D and
// has the same value for field s as this.s (w.r.t. String equals) and
// the same value for field g as this.g
// your code goes here
		if (o == null)
			return false;
		if (!this.getClass().equals(o.getClass()))
			return false;


		D that = (D) o;
		if (!this.s.equals(that.s))
			return false;

		return this.g == that.g;
	}

	@Override
	public int hashCode() {
// returns an integer consistent with the Java contract for hashCode method;
// does not return a constant value
// your code goes here
		return super.hashCode() + g;
	}
}