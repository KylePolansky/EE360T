package pset4;

import java.util.*;

public class Graph {
	private Set<Integer> nodes; // set of nodes in the graph
	private Map<Integer, List<Integer>> edges;
	// map between a node and a list of nodes that are connected to it by outgoing edges
// class invariant: fields "nodes" and "edges" are non-null;
// "this" graph has no node that is not in "nodes"
	public Graph() {
		nodes = new HashSet<Integer>();
		edges = new HashMap<Integer, List<Integer>>();
	}
	public String toString() {
		return "nodes=" + nodes + "; " + "edges=" + edges;
	}
	public void addNode(int n) {
		// postcondition: adds the node "n" to this graph

		nodes.add(n);
	}
	public void addEdge(int from, int to) {
		// postcondition: adds a directed edge "from" -> "to" to this graph

		//Add nodes if they don't exist
		addNode(from);
		addNode(to);

		//Add to list
		List<Integer> listOfEdges = edges.getOrDefault(from, new ArrayList<Integer>() {});
		listOfEdges.add(to);

		//Add the updated list to the edges graph
		edges.put(from, listOfEdges);
	}
	public boolean unreachable(Set<Integer> sources, Set<Integer> targets) {
		if (sources == null || targets == null)
			throw new IllegalArgumentException();

		// postcondition: returns true if (1) "sources" is a subset of "nodes", (2) "targets" is
		// a subset of "nodes", and (3) for each node "m" in set "targets",
		// there is no node "n" in set "sources" such that there is a
		// directed path that starts at "n" and ends at "m" in "this"; and
		// false otherwise


		//Condition 1
		if (!nodes.containsAll(sources))
			return false;

		//Condition 2
		if (!nodes.containsAll(targets))
			return false;

		//Condition 3
		return !sources.parallelStream()
				.anyMatch(s -> targets.parallelStream()
						.anyMatch(t -> doesPathExist(s, t, new HashSet<Integer>())));
	}

	private boolean doesPathExist(int start, int end, HashSet<Integer> visitedNodes) {
		//Ensure there are no recursive loops
		if (!visitedNodes.add(start)) {
			return false;
		}

		//Get list of edges associated with start
		List<Integer> startEdges = edges.get(start);
		if (startEdges == null) {
			return false;
		}

		//If one of these edges leads to end, return true
		if (startEdges.contains(end)) {
			return true;
		}

		//Else, recursively call for all combinations of edges.
		for (int edgeEnd : startEdges) {
			if (doesPathExist(edgeEnd, end, visitedNodes)) {
				return true;
			}
		}

		//If no paths found, return false
		return false;
	}
}
