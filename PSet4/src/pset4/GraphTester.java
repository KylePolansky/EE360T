package pset4;

import static org.junit.Assert.*;
import java.util.HashSet;
import java.util.Set;
import org.junit.Test;

public class GraphTester {
	// tests for method "addEdge" in class "Graph"
	@Test public void tae0() {
		Graph g = new Graph();
		g.addEdge(0, 1);
		assertEquals(g.toString(), "nodes=[0, 1]; edges={0=[1]}");
	}
	// your tests for method "addEdge" in class "Graph" go here
	// you must provide at least 4 test methods;
	// each test method must have at least 1 invocation of addEdge;
	// each test method must have at least 1 test assertion;
	// your test methods must provide full statement coverage of your
	// implementation of addEdge and any helper methods
	// no test method directly invokes any method that is not
	// declared in the Graph class as given in this homework

	@Test
	public void tae1() {
		Graph g = new Graph();
		g.addEdge(0, 1);
		g.addEdge(0, 2);
		assertEquals(g.toString(), "nodes=[0, 1, 2]; edges={0=[1, 2]}");
	}

	@Test
	public void tae2() {
		Graph g = new Graph();
		g.addEdge(0, 0);
		assertEquals(g.toString(), "nodes=[0]; edges={0=[0]}");
	}

	@Test
	public void tae3() {
		Graph g = new Graph();
		g.addEdge(0, 1);
		g.addEdge(0, 1);
		assertEquals(g.toString(), "nodes=[0, 1]; edges={0=[1, 1]}");
	}

	@Test
	public void tae4() {
		Graph g = new Graph();
		g.addEdge(0, 1);
		g.addEdge(2, 3);
		assertEquals(g.toString(), "nodes=[0, 1, 2, 3]; edges={0=[1], 2=[3]}");
	}





// tests for method "unreachable" in class "Graph"
	@Test
	public void tr0() {
		Graph g = new Graph();
		g.addNode(0);
		Set<Integer> nodes = new HashSet<Integer>();
		nodes.add(0);
		assertTrue(g.unreachable(new HashSet<Integer>(), nodes));
	}

	@Test(expected = IllegalArgumentException.class)
	public void tr1() {
		Graph g = new Graph();
		//next line should assert exception
		g.unreachable(null, new HashSet<>());
	}

	@Test(expected = IllegalArgumentException.class)
	public void tr2() {
		Graph g = new Graph();
		//next line should assert exception
		g.unreachable(null, new HashSet<>());
	}

	@Test
	public void tr3() {
		Graph g = new Graph();
		Set<Integer> nodes = new HashSet<>();
		nodes.add(0);
		assertFalse(g.unreachable(nodes, new HashSet<>()));
	}

	@Test
	public void tr4() {
		Graph g = new Graph();
		Set<Integer> nodes = new HashSet<>();
		nodes.add(0);
		assertFalse(g.unreachable(new HashSet<>(), nodes));
	}

	@Test
	public void tr5() {
		Graph g = new Graph();
		assertTrue(g.unreachable(new HashSet<>(), new HashSet<>()));
	}

	@Test
	public void tr6() {
		Graph g = new Graph();
		g.addEdge(0,1);
		Set<Integer> source = new HashSet<>();
		source.add(0);
		Set<Integer> targets = new HashSet<>();
		targets.add(1);

		assertFalse(g.unreachable(source, targets));
	}

	@Test
	public void tr7() {
		Graph g = new Graph();
		g.addEdge(0,0);
		g.addEdge(1,1);
		Set<Integer> source = new HashSet<>();
		source.add(0);
		Set<Integer> targets = new HashSet<>();
		targets.add(1);

		assertTrue(g.unreachable(source, targets));
	}

	@Test
	public void tr8() {
		Graph g = new Graph();
		g.addNode(0);
		g.addNode(1);

		Set<Integer> source = new HashSet<>();
		source.add(0);
		Set<Integer> targets = new HashSet<>();
		targets.add(1);

		assertTrue(g.unreachable(source, targets));
	}

	@Test
	public void tr9() {
		Graph g = new Graph();
		g.addEdge(0, 1);
		g.addEdge(1, 2);

		Set<Integer> source = new HashSet<>();
		source.add(0);
		Set<Integer> targets = new HashSet<>();
		targets.add(2);

		assertFalse(g.unreachable(source, targets));
	}

	@Test
	public void tr10() {
		Graph g = new Graph();
		g.addEdge(0, 1);
		g.addEdge(1, 2);
		g.addEdge(0, 3);

		Set<Integer> source = new HashSet<>();
		source.add(1);
		source.add(2);
		Set<Integer> targets = new HashSet<>();
		targets.add(3);

		assertTrue(g.unreachable(source, targets));
	}



// your tests for method "unreachable" in class "Graph" go here
// you must provide at least 6 test methods;
// each test method must have at least 1 invocation of unreachable;
// each test method must have at least 1 test assertion;
// at least 2 test methods must have at least 1 invocation of addEdge;
// your test methods must provide full statement coverage of your
// implementation of unreachable and any helper methods
// no test method directly invokes any method that is not
// declared in the Graph class as given in this homework
}