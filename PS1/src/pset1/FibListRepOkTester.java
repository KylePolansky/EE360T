package pset1;
import static org.junit.Assert.*;
import static pset1.FibList.*;

import org.junit.Test;


public class FibListRepOkTester {
	@Test public void t0() {
		FibList l = new FibList();
		l.size=2;
		assertFalse(l.repOk());
	}

	@Test public void t1() {
		FibList l = new FibList();
		l.size=4;
		assertFalse(l.repOk());
	}

	@Test public void t2() {
		FibList l = new FibList();
		assertTrue(l.repOk());
	}

	@Test public void t3() {
		FibList l = new FibList();
		Node n = new Node();
		n.elem = 1;
		l.header.next.next.next = n;
		assertFalse(l.repOk());
	}

	@Test public void t4() {
		FibList l = new FibList();
		l.header.elem = 0;
		l.header.next.elem = 0;
		l.header.next.next.elem = 0;
		l.header.next.next.next = l.header;
		assertFalse(l.repOk());
	}
}